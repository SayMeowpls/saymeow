#!/bin/bash
if [ $# -ne '0' ]
then
if [ $(pgrep $1) > '0' ]
then
echo "$1 запущен" >> /home/saymeow/lab4cron/log.txt
echo "`date`" >> /home/saymeow/lab4cron/log.txt
echo "-----------------------------------" >> /home/saymeow/lab4cron/log.txt
else
echo "$1 не был запущен, исправлено" >> /home/saymeow/lab4cron/log.txt
echo "`date`" >> /home/saymeow/lab4cron/log.txt
echo "-----------------------------------" >> /home/saymeow/lab4cron/log.txt
echo "`$1`"
fi
else
echo "Выберете действие"
echo "1 Задать новый отслеживаемый процесс"
echo "2 Удалить имеющийся"
echo "3 Изменить"
echo "4 Выход"
path=(`pwd`)
read doing
case $doing in
1)
echo "Введите название процесса"
read name
echo "Введите интервал проверки в минутах 0-59"
read min
echo "*/$min * * * * bash $path/$0 "$name"" >> cron-file.txt
echo "`crontab cron-file.txt`"
;;
2)max=(`wc -l cron-file.txt`)
echo "Выберете время из списка "
for ((s=4; s <= max ; s++))
do
f=3
echo "mm-hh--------------------$[$s-$f]-------------------------"
sed -n $s"p" cron-file.txt
done
echo "Какой из процессов удалить?"
read t
sed $[$t+$f]"d" cron-file.txt > cron-file2.txt
cp /dev/null cron-file.txt
cat cron-file2.txt > cron-file.txt
cp /dev/null cron-file2.txt
`crontab cron-file.txt`
;;
3)
max=(`wc -l cron-file.txt`)
echo "Выберете процесс из списка для изменения"
for ((s=4; s <= max ; s++))
do
f=3
echo "mm-hh--------------------$[$s-$f]-------------------------"
sed -n $s"p" cron-file.txt
done
read t
sed $[$t+$f]"d" cron-file.txt 1>cron-file2.txt
cp /dev/null cron-file.txt
cat cron-file2.txt > cron-file.txt
cp /dev/null cron-file2.txt
echo "Введите название процесса"
read name
echo "Введите интервал проверки в минутах 0-59"
read min
echo "*/$min * * * * bash $path/$0 "$name"" >> cron-file.txt
echo "`crontab cron-file.txt`"
;;
esac
fi
exit 0
