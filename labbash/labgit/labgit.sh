#!/bin/bash
echo "Выберете действие с GIT"
echo "1 Создать новый репозиторий"
echo "2 Удалить имеющийся репозиторий"
echo "3 Создать ветку в репозиторие"
echo "4 Удалить ветку"
echo "5 Переключиться на существующую ветку"
echo "6 Слить ветку с веткой master"
echo "7 Добавить файл в следующий коммит"
echo "8 Сделать commit"
echo "9 выход"
read doing
case $doing in
1)
echo "Введите путь к проекту"
read h
cd $h
git init
;;
2)
echo "Введите путь к репозитурию"
read d
rm -R $d"/.git"
;;
3)
echo "Введите путь к репозитурию"
read d
echo "Введите название новой ветки"
read f
cd $d
git branch $f
;;
4)
echo "Введите путь к репозитурию"
read d
echo "Введите название ветки которую следует удалить"
read f
cd $d
git branch -d $f
;;
5)
echo "Введите путь к репозитурию"
read d
echo "Введите название ветки на которую хотите перейти"
read f
cd $d
git checkout $f
;;
6)
echo "Введите путь к репозитурию"
read d
echo "Введите название ветки которую хотите слить с основной"
read f
cd $d
git checkout master
git merge $f
;;
7)
echo "Введите путь к репозитурию"
read d
echo "Введите название файла который хотите добавить"
read f
git add $f
;;
8)
echo "Введите путь к репозитурию"
read d
git commit
;;
esac
exit 0

