#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define RCVBUFSIZE 32   /* Size of receive buffer */
#define I 10
#define J 10
char fakemap[I][J];

void showmap()
{
    for (int k = 0; k < I; k++) {
        printf("\n\n");
        for (int l = 0; l < J; l++) {
            printf("%-5c",fakemap[k][l]);
        }
    }
    printf("\n\n");
}



int main(int argc, char *argv[])
{
    int hp2 = 0;
    int sock, recvMsgSize;           /* Socket descriptor */
    struct sockaddr_in ServAddr;     /* Echo server address */
    unsigned short ServPort;         /* Echo server port */
    char *servIP, step;                    /* Server IP address (dotted quad) */
//    char *String;                    /* String to send to echo server */
//    char Buffer[RCVBUFSIZE];         /* Buffer for echo string */
//    unsigned int StringLen;          /* Length of string to echo */
//    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv() 
 //                                       and total bytes read */

  
    
    int massbufsize = sizeof(fakemap);
    if ((argc < 2) || (argc > 3))    /* Test for correct number of arguments */ 
    {
       printf("Usage: %s <Server IP> <Echo Port>\n", argv[0]);
       exit(1);
    }

    servIP = argv[1];             /* First arg: server IP address (dotted quad) */
    ServPort = atoi(argv[2]);     /* Use given port, if any */
    
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        printf("socket() failed");
    
            /* Construct the server address structure */
    memset(&ServAddr, 0, sizeof(ServAddr));             /* Zero out structure */
    ServAddr.sin_family      = AF_INET;             /* Internet address family */
    ServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    ServAddr.sin_port        = htons(ServPort);     /* Server port */
    
    char hp4[4];
    
        /* Establish the connection to the echo server */
    if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
        printf("connect() failed\n");
    for (;;) {
        if ((recvMsgSize = recv(sock, fakemap, massbufsize, 0)) < 0)
            printf("recv1 failed\n");
        if ((recvMsgSize = recv(sock, hp4, sizeof(hp4), 0)) < 0) 
            printf("recv2 failed\n");
        showmap();
        int res = *((int*)hp4);
        printf("У вас %d HP, ваш ход\n", res);
        scanf("\n%c", &step);
        if (send(sock, &step, sizeof(step), 0) != sizeof(step))
            printf("send step error");
        if ((recvMsgSize = recv(sock, fakemap, massbufsize, 0)) < 0)
            printf("recv1 failed\n");
        showmap();
        printf("Ход соперника\n");
    }
    close(sock);
}
    
