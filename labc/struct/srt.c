#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
typedef struct spis /* typedef spis asdasd????*/
{
	char *name;
	int year;
	int number;
	int oklad;
} sp;

int yearcmp( const void * a, const void * b);
int main()
{	
	char name[40];
	int n;
	int year1, number1, oklad1;
	sp *sr;
	printf("Введите количество рабочих\n");
	scanf("%d", &n); /* количество работников */
	sr=malloc(sizeof(sp)*n); /* выделение памяти на количесвто структур */
	/* считываем список работников */
	for (int i=0; i < n ; i++) {
		printf("Введите фамилию работника\n");
		scanf("%s", name);
		sr[i].name=malloc(strlen(name)*sizeof(char)); /* Выделение памяти на длинну имени */
		strcpy(sr[i].name, name);
		printf("Введите год рождения\n");
		scanf("%d",&year1);
		printf("Введите номер отдела\n");
		scanf("%d", &number1);
		printf("Введите оклад\n");
		scanf("%d", &oklad1);
		sr[i].year=year1;
		sr[i].number=number1;
		sr[i].oklad=oklad1;
	}
	qsort(sr, n, sizeof(sp), yearcmp);
	printf("Фамилия Год Отдел Оклад\n");
	for (int i=0 ; i<n ; i++) {
		printf("%s%5d%5d%5d\n",sr[i].name,sr[i].year,sr[i].number,sr[i].oklad);
	}
	for (int i=0; i<n ; i++) { //очистка памяти
		free(sr[i].name);
	}
	free(sr);
	return 0; /* до сюда */
}
int yearcmp(const void* a, const void* b) {
	sp *pa=(sp *)a;
	sp *pb=(sp *)b;
	return (pa->year - pb->year);
}
