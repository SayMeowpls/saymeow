#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>

#define MSGPERM 0600    // msg queue permission
#define MSGTXTLEN 128   // msg text length

extern int errno; 
int contrlsumm(char * filename)
{
	FILE *fp;
	char ch;
	long summ=0;
	if ((fp = fopen(filename, "r"))==NULL) {
		printf("He удается открыть файл.\n");
		fflush(stdout);
		exit(1);
	}
	while((ch=fgetc(fp)) != EOF) {
		summ=summ+ch;
	}
	if(fclose(fp)) printf("Ошибка при закрытии файла.\n");
	fflush(stdout);
	return summ;
}


struct msg_buf {
	long mtype;
	char mtext[MSGTXTLEN];
} msg;


int main(int argc, char **argv) {
	int msgqid, rc, i;
	int pid[argc-1];
	int answer;
	msgqid = msgget(IPC_PRIVATE, MSGPERM|IPC_CREAT|IPC_EXCL); //Создание очереди
	if (msgqid < 0) {
		perror(strerror(errno));
		printf("failed to create message queue with msgqid = %d\n", msgqid);
		fflush(stdout);
		return 1;
	}
	printf("message queue %d created\n",msgqid);
	fflush(stdout);
	for (i=0; i < argc-1; i++){
		pid[i] = fork(); 
		if (pid[i] == 0){
			answer = contrlsumm(argv[i+1]);
			msg.mtype = 1; // set the type of message
			sprintf (msg.mtext, "Контрольная сумма равна %d\n", answer); /* setting the right time format by means of ctime() */
			fflush(stdout);
			rc = msgsnd(msgqid, (struct msg_buf *) &msg, sizeof(msg.mtext), 0); // the last param can be: 0, IPC_NOWAIT, MSG_NOERROR, or IPC_NOWAIT|MSG_NOERROR. WHY <0????????						
			if (rc < 0) {
				perror( strerror(errno) );
				printf("msgrcv failed, rc=%d\n", rc);
				fflush(stdout);
				return 1;
			}
			exit(0);
		}
		else if (pid[i] < 0){
			printf("ERROR fork");
			fflush(stdout);
		}
		else if(pid[i] > 0){
			rc = msgrcv(msgqid, &msg, sizeof(msg.mtext), 0, 0); 
			if (rc < 0) {
				perror( strerror(errno) );
				printf("FAIL rcv, rc=%d\n", rc);
				fflush(stdout);
				return 1;
			}
			printf("received msg: %s\n", msg.mtext);
			fflush(stdout);
		}
	}
	rc=msgctl(msgqid,IPC_RMID,NULL);
	printf("msgctl completed rc=%d\n", rc);
	fflush(stdout);
	if (rc < 0) {
		perror( strerror(errno) );
		printf("msgctl (return queue) failed, rc=%d\n", rc);
		fflush(stdout);
		return 1;		
	}
}
