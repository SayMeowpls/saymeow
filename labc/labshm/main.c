#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sem.h>
#include <sys/types.h>
#include "semun.c"

#define MSGPERM 0600    // msg queue permission
#define MSGTXTLEN 128   // msg text length

extern int errno;
struct sembuf lock_res = {0, -1, 0};
struct sembuf rel_res = {0, 1, 0};
int semid;

int honeybee(int med){
    printf("Пчела в функции");
    sleep(5);
    int total = rand()%5+10;
    med += total;
    printf("(PID: %d) Пчелка принесла = %d меда\n", getpid(), total);
    printf("Количество меда увеличилось, и стало = %d\n",med);
    return med;
}  
int bear(int med){
    printf("Медведь в функции");
    sleep(3);
    int total = rand()%5+20;
    med -= total;
    printf("(PID: %d) Медведь забрал  = %d меда\n", getpid(), total);
    printf("Количество меда уменьшилось, и стало = %d\n",med);
    return med;
}

int main(int argc, char **argv){
    srand(getpid());
    int stat1, status1, status2, stat2, pid[2];
    int answer, answer2, id, oflag, rc, rm;
    struct shmid_ds buff;
    int length = 256;
    int *ptr;
    key_t key;
    union semun arg;
    key = ftok("/etc/fstab", getpid());
    semid = semget(key, 1, 0666 | IPC_CREAT);
    oflag = 0666 | IPC_CREAT;
    oflag |= IPC_EXCL;
    printf("Semaphore: %d\n",semid);
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);
    id = shmget(IPC_PRIVATE, length, oflag);
    ptr = shmat(id, NULL, 0);
    shmctl(id, IPC_STAT, &buff);
    *ptr = 500;
    printf("shared memory %d created\n",id);
    fflush(stdout);
    
    pid[0] = fork();
    if (pid[0] == 0){
        printf("id пчелы = %d ptr=%p\n", id, ptr);
        for (int j = 0; j < 100; j++) {
            printf("j=%d\n", j);
            sleep(rand()%5);
            if (semop(semid, &lock_res, 1) == -1) {
            printf("улей занят медведем");
            }
            answer = honeybee(*ptr);
            *ptr = answer;    
            semop(semid, &rel_res, 1);        
        }
            exit(0);
    }
    else if (pid[0] < 0) {
        printf("ERROR fork 0");
        fflush(stdout);
        }
    pid[1] = fork();
    if (pid[1] == 0){
        printf("id медведя = %d , ptr= %p\n", id, ptr);
        for (int k = 0; k < 100; k++) {
            printf("k=%d\n", k);
            sleep(rand()%5);
            if (semop(semid, &lock_res, 1) == -1) {
            printf("улей занят медведем");
            }
            answer2 = bear(*ptr);
            *ptr = answer2;
            semop(semid, &rel_res, 1);
        }
        exit(0);
    }
    else if (pid[1] < 0){
        printf("ERROR fork 1");
        fflush(stdout);
    }
    printf("Количество меда %d\n", *ptr);
    fflush(stdout);
    status1 = waitpid(pid[1],&stat1,WEXITSTATUS(stat1));
    status2 = waitpid(pid[0],&stat2,WEXITSTATUS(stat2));
    printf("status2=%d\n", status2);
    if ( pid[1] == status1 && pid[0] == status2 ) {  
        rc=shmctl(id, IPC_RMID, NULL);
        printf("shm delete rc=%d\n", rc);
        if (rc < 0) {
            perror( strerror(errno) );
            printf("shm delete failed, rc=%d\n", rc);
            return 1;		
        }
        printf("shmctl completed rc=%d\n", rc);
        rm = semctl(semid, 0, IPC_RMID);
        printf("sem delete rm=%d\n", rm);
        if (rm < 0) {
            perror( strerror(errno) );
            printf("sem delete failet, rm=%d\n", rm);
            return 1;		
        }
    }
}
