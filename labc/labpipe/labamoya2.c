#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "filelab.c"
#define MAXLINE 80
int main(int argc, char *argv[]) 
{
	ssize_t n;
	int i, status, stat, amount, max;
	char buff;
	printf("Введите количество шахтеров\n");
	scanf("%d",&amount);
	int pid[amount];
	printf("Введите количество золота в шахте\n");
	scanf("%d",&max);
	int pipe1[amount][2],pipe2[amount][2];
	while (max > 15) {
		for (i=0; i< amount; i++) {
			pipe(pipe1[i]);
			pipe(pipe2[i]);
			if (amount<1) {
				printf("Необходимо ввести количество шахтеров\n");
				exit(-1);
			}
			pid[i] = fork(); 
			if (pid[i] == 0) {
				close(pipe2[i][0]);
				if (max > 15)
				filelab(pipe2[i][1]);
				exit(0);
			}
			else
			if (pid[i] < 0) {
				printf("ERROR");
			}
			else {
				close(pipe2[i][1]);
				sleep(1);
				status = waitpid(pid[i],&stat,WNOHANG);
				if (pid[i] == status) {
					while ( (n= read(pipe2[i][0], &buff, MAXLINE)) > 0) {
						int ostatok=max-buff;
						max=ostatok;
						printf("После шахтера %d В шахте осталось %d золота\n",i+1, max);
					}
				}
			}
		}
	}
	return 0; 
}
