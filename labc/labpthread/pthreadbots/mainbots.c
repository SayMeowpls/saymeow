#include <unistd.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define I 10
#define J 10

int map[I][J];
char fakemap[I][J];
int number_of_bots = 4;
static int j = -1;

void createmap()
{
    for (int k = 0; k < I; k++) {
        for (int l = 0; l < J; l++) {
            map[k][l]=(1-rand()%3);
        }
    }
}

typedef struct use_mutex_tag {
    pthread_mutex_t mutex;
} use_mutex_t;

void* bot(void *args)
{
    int x = rand()%10;
    int y = rand()%10;
    int hp = 2;
    map[x][y] = 0;
    fakemap[x][y] = 'A';
    use_mutex_t *arg = (use_mutex_t*) args;
    while (hp > 0) {
        printf("hp в начале равно %d\n", hp);
        int botstep = rand()%4;
        fakemap[x][y] = 'O';
        botstep == 0 ? (x -= 1) : botstep == 1 ? (x += 1) : botstep == 2 ? (y += 1) : botstep == 3 ? (y -= 1) : printf("error\n") ;
        if (x > 9 || x < 0 || y < 0 || y > 9 ){
            hp = 0;
            printf("Бот застрял в текстурах и погиб :с\n");
        }
        fakemap[x][y] = 'A';
        pthread_mutex_lock(&(arg->mutex));
        hp += map[x][y];
        map[x][y] = 0;
        showmap();
        pthread_mutex_unlock(&(arg->mutex));
        printf("hp в конце равно %d\n", hp);
        sleep(3);
    }
    return NULL;
}

int createfakemap()
{
    for (int k = 0; k < I; k++) {
        for (int l = 0; l < J; l++) {
            fakemap[k][l] = 'X';
        }
    }
}   

void showmap()
{
    system("clear");
    for (int k = 0; k < I; k++) {
        printf("\n\n");
        for (int l = 0; l < J; l++) {
            printf("%-5c",fakemap[k][l]);
        }
    }
    printf("\n\n");
}

int main() {
    pthread_t threads[number_of_bots];
    use_mutex_t param;
    pthread_mutex_init(&(param.mutex), NULL);
    createmap();
    createfakemap();
    for (int j = 0; j < number_of_bots; j++) {
        pthread_create(&threads[j], NULL, bot, &param);
    }
    for (int i = 0; i < number_of_bots; i++) {
        pthread_join(threads[i], NULL);
    }
    pthread_mutex_destroy(&(param.mutex));
    return (EXIT_SUCCESS);
}