#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define MAXPENDING 5    /* Maximum outstanding connection requests */
#define I 10
#define J 10

int map[I][J];
char fakemap[I][J];

void createmap()
{
    for (int k = 0; k < I; k++) {
        for (int l = 0; l < J; l++) {
            map[k][l]=(1-rand()%3);
        }
    }
}

int createfakemap()
{
    for (int k = 0; k < I; k++) {
        for (int l = 0; l < J; l++) {
            fakemap[k][l] = 'X';
        }
    }
}

void showmap()
{
    for (int k = 0; k < I; k++) {
        printf("\n\n");
        for (int l = 0; l < J; l++) {
            printf("%-5c",fakemap[k][l]);
        }
    }
    printf("\n\n");
}

int main(int argc, char** argv) {
    
    int hp = 2, x, y, x2, y2, i = 0, recvMsgSize;
    char hp2[4];
    *((int*)hp2) = 2;
    char step, step2;
    srand(getpid());
    x = rand()%10;
    y = rand()%10;
    x2 = rand()%10;
    y2 = rand()%10;
    createmap();
    createfakemap(I,J);
    fakemap[x][y] = 'A';
    fakemap[x2][y2] = 'B';
    map[x][y] = 0;
    map[x2][y2] = 0;
    int fakemapsize = sizeof(fakemap);
    
    int ServSock;                    /* Socket descriptor for server */
    int clntSock;                    /* Socket descriptor for client */
    struct sockaddr_in ServAddr; /* Local address */
    struct sockaddr_in ClntAddr; /* Client address */
    unsigned short ServPort;     /* Server port */
    unsigned int clntLen;            /* Length of client address data structure */

    if (argc != 2)     /* Test for correct number of arguments */
    {
        printf("Usage:  %s <Server Port>\n", argv[0]);
        exit(1);
    }
    ServPort = atoi(argv[1]);  /* First arg:  local port */
    
        /* Create socket for incoming connections */
    if ((ServSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        printf("socket() failed");
      
    memset(&ServAddr, 0, sizeof(ServAddr));   /* Zero out structure */
    ServAddr.sin_family = AF_INET;                /* Internet address family */
    ServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    ServAddr.sin_port = htons(ServPort);      /* Local port */

    if (bind(ServSock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
        printf("bind() failed");
    
    if (listen(ServSock, MAXPENDING) < 0)
        printf("listen() failed");

    for (;;) { /* Run forever */ 
        clntLen = sizeof(ClntAddr);
        if ((clntSock = accept(ServSock, (struct sockaddr *) &ClntAddr, &clntLen)) < 0) {
            printf("accept() failed");
        }
        while (hp > 0 && *((int*)hp2) > 0) {
            i+=1;
            if(i%2 == 1) {
                showmap();
                printf("Ход соперника\n");
                if (send(clntSock, fakemap, fakemapsize, 0) != fakemapsize)
                    printf("send map failed\n");
                if (send(clntSock, hp2, sizeof(hp2), 0) != sizeof(hp2))
                    printf("send hp failed\n");
                if ((recvMsgSize = recv(clntSock, &step2, sizeof(char), 0)) < 0)
                    printf("recv() failed\n");
                fakemap[x2][y2] = 'O';
                step2 == 'w' ? (x2 -= 1) : step2 == 's' ? (x2 += 1) : step2 == 'd' ? (y2 += 1) : step2 == 'a' ? (y2 -= 1) : printf("error\n") ;
                if (x2 > 9 || x2 < 0 || y2 < 0 || y2 > 9 ){
                    printf("Застрял в текстурах\n");
                    *((int *)hp2) = 0;
                }
                fakemap[x2][y2] = 'B';
                *((int*)hp2) += map[x2][y2];
                map[x2][y2] = 0;
                if (send(clntSock, fakemap, fakemapsize, 0) != fakemapsize)
                    printf("send map failed\n");
            }
            if(i%2 == 0) {
                showmap();
                printf("Ходит А,i=%d\n", i);
                printf("У вас %d HP, сделайте ход\n", hp);
                scanf("\n%c", &step);
//                printf("step=%c\n", step);
                fakemap[x][y] = 'O';
                step == 'w' ? (x -= 1) : step == 's' ? x += 1 : step == 'd' ? y += 1 : step == 'a' ? y -= 1 : printf("error\n") ;
                if (x > 9 || x < 0 || y < 0 || y > 9 ){
                    printf("Застрял в текстурах\n");
                    hp = 0;
                }
                fakemap[x][y] = 'A';
                hp += map[x][y];
                map[x][y] = 0;
            }
            if ( x == x2 && y == y2 ) {
                ((hp - *((int*)hp2) > 0)) ? printf("Победил А") : printf("Победил B");
                hp = 0;
            }
        }
        (hp == 0) ? printf("Победил А") : printf("Победил B");
        close(clntSock);
    }
    return (EXIT_SUCCESS);
}
