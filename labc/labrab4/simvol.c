#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define N 80
int main(int argc, char *argv[])
{
	FILE *in, *out;
	char ch;
	if(argc!=4) {
		printf("Вы забыли ввести имя файла.\n");
		exit(1);
	}
	if((in=fopen(argv[1], "rb"))==NULL) {
		printf("Нельзя открыть исходный файл.\n");
		exit(1);
	}
	if((out=fopen(argv[2], "wb")) == NULL) {
		printf("Нельзя открыть файл результатов.\n");
		exit(1);
	}

	/* Именно этот код копирует файл. */
	while(!feof(in)) {
		ch=getc(in);
		if(ch != *argv[3])
		if(!feof(in)) putc(ch, out);
	}
	fclose(in);
	fclose(out);
	return 0;
}
