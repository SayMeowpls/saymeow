#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define N 80
int main(int argc, char *argv[])
{
	FILE *in, *out;
	char str[N];
	int k=0;
	if(argc!=4) {
		printf("Вы забыли ввести имя файла.\n");
		exit(1);
	}

	if((in=fopen(argv[1], "rb"))==NULL) {
		printf("Нельзя открыть исходный файл.\n");
		exit(1);
	}
	if((out=fopen(argv[2], "wb")) == NULL) {
		printf("Нельзя открыть файл результатов.\n");
		exit(1);
	}

	/* Именно этот код копирует файл. */
	while(!feof(in)) {
		fgets(str, N, in);
		k=0;
		for (int i=0; i<N; i++) {
			if(str[i]==' ')
			k=k+1;
		}
	printf("%s",str);
	printf("%d\n",k);
	if (k < atoi(argv[3]))
	if(!feof(in)) fputs(str, out);
	}
	fclose(in);
	fclose(out);
	return 0;
}
