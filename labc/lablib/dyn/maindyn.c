#include <stdio.h>
#include <dlfcn.h>

int kv, rez1;
float ko, rez2;
int main(int argc, char **argv) {
	void *ext_library;
	void *ext_library1;
	int (*powerfunc)(int x);
	float (*powerfunc1)(int x);
	ext_library = dlopen("/home/saymeow/eltex/labc/lablib/dyn/libkvadr.so", RTLD_LAZY); 
	if (!ext_library) { //если ошибка
		fprintf(stderr,"dlopen() error: %s\n", dlerror()); 
		return(1);
	}
	ext_library1 = dlopen("/home/saymeow/eltex/labc/lablib/dyn/libkoren.so", RTLD_LAZY); 
	if (!ext_library1) { //если ошибка
		fprintf(stderr,"dlopen() error: %s\n", dlerror()); 
		return(1);
	}
	printf("Libraries loaded!\n");
	powerfunc = dlsym(ext_library, "kvadr");
	powerfunc1 = dlsym(ext_library1, "koren");
	printf("Введите число для возведения в квадрат\n");
	scanf("%d", &kv);
	printf("Введите число для нахождения его корня\n");
	scanf("%f", &ko);
	rez1=(*powerfunc)(kv);
	rez2=(*powerfunc1)(ko);
	printf("Квадрат заданного числа = %d\n", rez1);
	printf("Корень заданного числа = %f\n", rez2);
	dlclose(ext_library);
	dlclose(ext_library1);
	return 0;
}
