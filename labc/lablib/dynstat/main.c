#include <stdio.h>
extern int kvadr(int);
extern float koren(int);
int kv, rez1;
float ko, rez2;
int main(int argc, char **argv) {
	printf("Введите число для возведения в квадрат\n");
	scanf("%d", &kv);
	printf("Введите число для нахождения его корня\n");
	scanf("%f", &ko);
	rez1=kvadr(kv);
	rez2=koren(ko);
	printf("Квадрат заданного числа =%d\n", rez1);
	printf("Корень заданного числа =%f\n", rez2);
	return 0;
}

