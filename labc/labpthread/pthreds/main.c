/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: saymeow
 *
 * Created on 3 марта 2017 г., 22:09
 */
#include <unistd.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define I 10
#define J 10

int map[I][J];
char fakemap[I][J];
char m = 'X';
void createmap()
{
    for (int k = 0; k < I; k++) {
        for (int l = 0; l < J; l++) {
            map[k][l]=(1-rand()%3);
        }
    }
}

int createfakemap()
{
    for (int k = 0; k < I; k++) {
        for (int l = 0; l < J; l++) {
            fakemap[k][l] = 'X';
        }
    }
}
void showmap()
{
    for (int k = 0; k < I; k++) {
        printf("\n\n");
        for (int l = 0; l < J; l++) {
            printf("%-5c",fakemap[k][l]);
        }
    }
    printf("\n\n");
}
int main(int argc, char** argv) {
    int hp = 2, hp2 = 2, x, y, x2, y2, i = 0;
    char step, step2;
    srand(getpid());
    x = rand()%10;
    y = rand()%10;
    x2 = rand()%10;
    y2 = rand()%10;
    createmap();
    createfakemap(I,J);
    fakemap[x][y] = 'A';
    fakemap[x2][y2] = 'B';
    map[x][y] = 0;
    map[x2][y2] = 0;
    i = 0;
    while (hp > 0 && hp2 > 0) {
        i+=1;
        if(i%2 == 0) {
            printf("Ходит А,i=%d\n", i);
            showmap();
            printf("У вас %d HP, сделайте ход\n", hp);
            scanf("\n%c", &step);
            printf("step=%c\n", step);
            fakemap[x][y] = 'O';
            step == 'w' ? (x -= 1) : step == 's' ? (x += 1) : step == 'd' ? (y += 1) : step == 'a' ? (y -= 1) : printf("error\n") ;
            if (x > 9 || x < 0 || y < 0 || y > 9 ){
                printf("Застрял в текстурах\n");
                hp = 0;
            }
            fakemap[x][y] = 'A';
            hp += map[x][y];
            map[x][y] = 0;
        }
        else if (i%2 == 1) {
            printf("Ходит B,i=%d\n", i);
            showmap();
            printf("У вас %d HP, сделайте ход\n", hp2);
            scanf("\n%c", &step2);
            printf("step2=%c\n", step2);
            fakemap[x2][y2] = 'O';
            (step2 == 'w') ? (x2 -= 1) : (step2 == 's') ? (x2 += 1) : (step2 == 'd') ? (y2 += 1) : (step2 == 'a') ? (y2 -= 1) : (printf("err0r\n"));
            if (x2 > 9 || x2 < 0 || y2 < 0 || y2 > 9 ){
                printf("Застрял в текстурах\n");
                hp2 = 0;
            }
            fakemap[x2][y2] = 'B';
            hp2 += map[x2][y2];
            map[x2][y2] = 0;
        }
        if ( x == x2 && y == y2 ) {
            ((hp - hp2) > 0) ? printf("Победил А") : printf("Победил B");
            hp = 0;
        }
    }
    return (EXIT_SUCCESS);
}

