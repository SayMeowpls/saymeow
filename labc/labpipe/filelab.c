#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
void filelab(int writefd)
{
	ssize_t f;
	srand(getpid());
	int total = rand()%5+10;
	printf("(PID: %d) Шахтер добыл = %d\n", getpid(), total);
	f = sizeof(int);
	write(writefd, &total, f);
}
